.PHONY: main atlas

compile = g++ -std=c++14 -Wall -Werror -pedantic
optimised = -O3
debug = -ggdb
sfml = -lsfml-window -lsfml-graphics -lsfml-system
shared = -Wl,-Bdynamic
files = code/main.cpp code/tilemap.cpp code/atlas.cpp code/window.cpp

main:
	$(compile) $(optimised) $(files)            $(sfml) -o main
shared:
	$(compile) $(optimised) $(files) $(dynamic) $(sfml) -o main
debug:
	$(compile) $(debug)     $(files)            $(sfml) -o main
debug-shared:
	$(compile) $(debug)     $(files) $(dynamic) $(sfml) -o main
