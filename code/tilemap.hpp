#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

#include <string>
#include <vector>
#include <fstream>
#include <iostream>

#include "atlas.hpp"

namespace tilemap {
  class Tilemap : public virtual sf::Drawable {
  private:
  public:
    atlas::Atlas atlas;
    int mapWidth;
    int mapHeight;
    std::vector<std::vector<sf::Sprite>> tiles;
    Tilemap(std::string, std::string, unsigned int);
    void draw(sf::RenderTarget&, sf::RenderStates) const;
  };
}
