#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

#include <list>

namespace window {
  class Window {
  private:
    sf::View worldView;
    sf::View guiView;
    float zoomLevel;
  public:
    sf::RenderWindow window;
    std::list<sf::Drawable*> renderList;
    std::list<sf::Drawable*> guiList;
    void translate(float, float);
    void zoomBy(float);
    void zoom(float);
    float zoom();
    void draw();
    void resizeView(unsigned int, unsigned int);
    Window(unsigned int, unsigned int);
    Window() : Window(640, 640) {};
  };
}
