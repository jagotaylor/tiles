#include "atlas.hpp"

using namespace atlas;

Atlas::Atlas(unsigned int size, std::string filepath) {
  if (!textures.loadFromFile(filepath)) {
    throw new TextureNotFoundException;
  }
  this->size = size;
  sf::Vector2u tSize = textures.getSize();
  width = tSize.x/size;
  height = tSize.y/size;
}

void Atlas::setTexture(sf::Sprite& dest, unsigned int index) {
  int col = index % size;
  int row = index / size;
  sf::IntRect rect(col*size, row*size, size, size);
  dest.setTexture(textures);
  dest.setTextureRect(rect);
}

unsigned int Atlas::getSize() const {
  return size;
}
