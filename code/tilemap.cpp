#include "tilemap.hpp"

using namespace tilemap;

Tilemap::Tilemap(std::string filepath, std::string atlasPath, unsigned int size) : atlas(size, atlasPath) {
  std::ifstream file;
  file.open(filepath, std::ios::binary);
  mapWidth = file.get();
  mapHeight = file.get();
  for (int row = 0; row < mapHeight; row++) {
    tiles.emplace_back(mapWidth);
    for (int col = 0; col < mapWidth; col++) {
      atlas.setTexture(tiles[row][col], file.get());
      tiles[row][col].setPosition(sf::Vector2f(col*atlas.getSize(), row*atlas.getSize()));
    }
  }
  file.close();
}

void Tilemap::draw(sf::RenderTarget& target, sf::RenderStates state) const {
  sf::Vector2f size = target.getView().getSize();
  float width = size.x/atlas.getSize();
  float height = size.y/atlas.getSize();
  sf::Vector2f pos = target.getView().getCenter();
  int x = pos.x/atlas.getSize() - width/2;
  int y = pos.y/atlas.getSize() - height/2;

  for (int row = y; row <= y+height+1; row++) {
    for (int col = x; col <= x+width+1; col++) {
      if (row < 0) continue;
      if (row >= mapHeight) continue;
      if (col < 0) continue;
      if (col >= mapWidth) continue;
      target.draw(tiles[row][col], state);
    }
  }
}
