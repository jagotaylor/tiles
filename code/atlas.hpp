#ifndef ATLAS_INCLUDED
#define ATLAS_INCLUDED

#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

#include <string>

namespace atlas {
  class Atlas {
    unsigned int size;
    unsigned int width;
    unsigned int height;
    sf::Texture textures;
  public:
    Atlas(unsigned int, std::string);
    void setTexture(sf::Sprite&, unsigned int);
    unsigned int getSize() const;
  };

  struct TextureNotFoundException : public std::exception{
    virtual const char* what() const throw() {
      return "The texture was not found";
    }
  };
}

#endif // ATLAS_INCLUDED
