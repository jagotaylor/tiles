#include "atlas.hpp"
#include "tilemap.hpp"
#include "window.hpp"
#include "tween.hpp"

enum class tweenPoints {zoom};

int main() {

  window::Window window;
  window.zoom(1.5);

  int vel = 150;

  sf::Clock clock;
  float elapsed;

  tilemap::Tilemap tiles("code/large.map", "tiles/tiles.png", 64);
  window.renderList.push_back(&tiles);

  sf::Texture heartTexture;
  if (!heartTexture.loadFromFile("GUI/fullHeart.png")) {
    std::cout << "No heart!" << std::endl;
  }
  heartTexture.setSmooth(true);
  float xScale = 40.0 / heartTexture.getSize().x;
  float yScale = 40.0 / heartTexture.getSize().y;
  sf::Sprite heartSprite;
  heartSprite.setTexture(heartTexture);
  heartSprite.setScale(xScale, yScale);
  heartSprite.setPosition(4, 4);
  window.guiList.push_back(&heartSprite);

  tween::TweenList<tweenPoints> tweens;

  sf::Event event;
  while (window.window.isOpen()) {
    while (window.window.pollEvent(event)) {
      switch (event.type) {
        case sf::Event::Closed:
          window.window.close();
          break;
        case sf::Event::Resized:
          window.resizeView(event.size.width, event.size.height);
          break;
        case sf::Event::KeyPressed:
          switch (event.key.code) {
            case sf::Keyboard::R:
              vel = 400;
              break;
            case sf::Keyboard::Q:
              tweens.add(tweenPoints::zoom, [&window](float t){window.zoom(t);}, window.zoom(), 1.5);
              break;
            case sf::Keyboard::W:
              tweens.add(tweenPoints::zoom, [&window](float t){window.zoom(t);}, window.zoom(), 3.0);
              break;
            case sf::Keyboard::E:
              tweens.stop(tweenPoints::zoom);
              window.zoom(1.5);
              break;
            default:;
          }
          break;
        case sf::Event::KeyReleased:
          if (event.key.code == sf::Keyboard::R) { vel = 100; }
          break;
        default:;
      }
    }

    elapsed = clock.restart().asSeconds();

    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up)) {
      window.translate(0, -vel*elapsed);
    } else
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down)) {
      window.translate(0, vel*elapsed);
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left)) {
      window.translate(-vel*elapsed, 0);
    } else
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right)) {
      window.translate(vel*elapsed, 0);
    }

    tweens.tween(elapsed);
    window.draw();
  }
  return 0;
}
