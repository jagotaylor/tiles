#ifndef TWEEN_INCLUDED
#define TWEEN_INCLUDED

#include <functional>
#include <memory>
#include <map>

namespace tween {
  class Tweenable {
  public:
    virtual ~Tweenable() {};
    virtual void tween(float) = 0;
    virtual bool isDone() = 0;
  };

  class TweenValue : public Tweenable {
    float* value;
    float start;
    float progress;
    float end;
    float time;
    bool done;
  public:
    TweenValue(float&, float, float, float);
    void tween(float);
    bool isDone() { return done; };
  };

  class TweenFunction : public Tweenable {
    std::function<void(float)> function;
    float start;
    float progress;
    float end;
    float time;
    bool done;
  public:
    TweenFunction(std::function<void(float)>, float, float, float);
    void tween(float);
    bool isDone() { return done; };
  };

  template<typename Id>
  class TweenList {
    std::map<Id, std::unique_ptr<Tweenable>> tweens;
  public:
    void add(Id, std::function<void(float)>, float, float, float=1);
    void add(Id, float&, float, float, float=1);
    void tween(float);
    bool running(Id);
    void stop(Id);
  };
}

#include "tween.cpp"

#endif // TWEEN_INCLUDED
