#include "tween.hpp"

using namespace tween;

TweenValue::TweenValue(float& f, float start, float end, float time) {
  value = &f;
  this->start = start;
  this->end = end;
  this->time = time;
  this->done = false;
}

void TweenValue::tween(float t) {
  progress += t/time;
  if (progress > 1.0) {
    *value = end;
    done = true;
  } else {
    *value = (1 - progress) * start + progress * end;
  }
}


TweenFunction::TweenFunction(std::function<void(float)> f, float start, float end, float time) {
  function = f;
  this->start = start;
  this->end = end;
  this->time = time;
  this->done = false;
}

void TweenFunction::tween(float t) {
  progress += t/time;
  if (progress > 1.0) {
    done = true;
    function(end);
  } else {
    function((1 - progress) * start + progress * end);
  }
}


template<typename Id>
void TweenList<Id>::add(Id id, std::function<void(float)> f, float start, float end, float time) {
  if (tweens.count(id) == 0) {
    tweens.emplace(id, std::unique_ptr<TweenFunction>(new TweenFunction(f, start, end, time)));
  }
}

template<typename Id>
void TweenList<Id>::add(Id id, float& f, float start, float end, float time) {
  if (tweens.count(id) == 0) {
    tweens.emplace(id, std::unique_ptr<TweenValue>(new TweenValue(f, start, end, time)));
  }
}

template<typename Id>
void TweenList<Id>::tween(float t) {
  auto pair = tweens.begin();
  while (pair != tweens.end()) {
    pair->second->tween(t);
    if (pair->second->isDone()) {
      pair = tweens.erase(pair);
    } else {
      pair++;
    }
  }
}

template<typename Id>
bool TweenList<Id>::running(Id id) {
  try {
    return !tweens.at(id).second->isDone();
  } catch (std::out_of_range) { ; }
  return false;
}

template<typename Id>
void TweenList<Id>::stop(Id id) {
  tweens.erase(id);
}
