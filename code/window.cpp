#include "window.hpp"

using namespace window;

Window::Window(unsigned int width, unsigned int height) :
    worldView(sf::FloatRect(0, 0, width, height)),
    window(sf::VideoMode(width, height), "Hello!") {
  window.setVerticalSyncEnabled(true);
  zoomLevel = 1;
}

void Window::draw() {
  window.clear(sf::Color::Black);
  window.setView(worldView);
  for (sf::Drawable* object : renderList) {
    window.draw(*object);
  }
  window.setView(guiView);
  for (sf::Drawable* object : guiList) {
    window.draw(*object);
  }
  window.display();
}

void Window::resizeView(unsigned int width, unsigned int height) {
  worldView.setSize(width, height);
}

float Window::zoom() {
  return zoomLevel;
}

void Window::zoom(float targetZoom) {
  float factor = targetZoom/zoomLevel;
  worldView.zoom(factor);
  zoomLevel = targetZoom;
}

void Window::zoomBy(float factor) {
  worldView.zoom(factor);
  zoomLevel *= factor;
}

void Window::translate(float x, float y) {
  worldView.move(x, y);
}
